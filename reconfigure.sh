#!/bin/sh
# (Re)configure's meson build and link's compile_commands.json to
# project root.
BUILD_DIR="build"

if [ -d $BUILD_DIR ]; then
    meson setup --reconfigure $BUILD_DIR
else
    meson setup $BUILD_DIR
fi

if ! ( [ -L "compile_commands.json" ] ); then
    ln -s $BUILD_DIR/"compile_commands.json"
fi
