#include <OFSFile.hpp>
#include <fstream>
#include <iostream>
#include <utils.hpp>

#define HEADERSIZE 8
#define FPSVALUE 28  // I think
#define DEPTHPOS 41

float OFSFpsValue2Fps(unsigned char fpsValue);

OFSFile::OFSFile(const char *fileName) {
  // Initialize class variables.
  this->m_error = 0;
  this->m_dropframe = false;

  unsigned char OFSSig[HEADERSIZE] = {0x89, 0x4f, 0x46, 0x53,
                                      0x0d, 0x0a, 0x1a, 0x0a};

  // Read file into buffer.
  std::ifstream input(fileName, std::ios::binary);

  if (!input.good()) {
    std::cout << fileName << ": Doesn't exist.\n";
    this->m_error = 3;
    return;
  }
  std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(input), {});

  // Initialize m_fileName
  this->m_fileName = getBaseName(fileName);

  // Verify OFS file.
  bool isOFS = true;
  for (int x = 0; x < HEADERSIZE; x++) {
    if (OFSSig[x] != buffer[x]) {
      isOFS = false;
    }
  }

  // Dump the depth values to the vector m_depths,
  // and add fps value.
  // If not... Set the error number.
  if (isOFS) {
    // Initalize m_fps variable.
    this->m_fps = OFSFpsValue2Fps(buffer[FPSVALUE]);
    // 65 = fps is 29.97 and dropframe is set.
    if (buffer[FPSVALUE] == 65) {
      this->m_dropframe = true;
    }
    for (int x = DEPTHPOS; x < (int)buffer.size(); x++) {
      this->m_depths.push_back(buffer[x]);
    }
  } else {
    std::cout << "'" << fileName << "'"
              << " Is not an OFS file.\n";
    this->m_error = 3;  // Internal Error.
  }
}

// Prints depth stats.
void OFSFile::printOFSInfo() {
  int minVal = 128;
  int maxVal = -128;
  int total = 0;
  int undefined = 0;
  int firstFrame = -1;
  int lastFrame = -1;
  int lastVal = 0;
  int byte;
  int cuts = 0;

  for (int i = 0; i < (int)this->m_depths.size(); i++) {
    byte = this->m_depths[i];
    if (byte != lastVal) {
      cuts++;
      lastVal = byte;
    }
    if (byte == 128) {
      undefined++;
      continue;
    } else {
      lastFrame = i;
      if (firstFrame == -1) {
        firstFrame = i;
      }
    }

    // If the value is bigger then 128. Negate the value.
    if (byte > 128) {
      byte = 128 - byte;
    }

    if (byte < minVal) {
      minVal = byte;
    }
    if (byte > maxVal) {
      maxVal = byte;
    }
    total += byte;
  }

  std::string dropFrame = "False";
  if (this->m_dropframe) {
    dropFrame = "True";
  }

  double average =
      (double)total / ((double)this->m_depths.size() - (double)undefined);
  std::cout << "...OFS Info...\n\n"
            << "Filename: " << this->m_fileName << "\n"
            << "Framerate: " << noZeroFloat2Str(this->m_fps) << "\n"
            << "Dropframe: " << dropFrame << "\n\n"
            << "...3D-Plane Stats...\n\n"
            << "NumFrames: " << this->m_depths.size() << "\n"
            << "Minimum depth: " << minVal << "\n"
            << "Maximum depth: " << maxVal << "\n"
            << "Average depth: " << average << "\n"
            << "Number of changes of depth value: " << cuts << "\n"
            << "First frame with a defined depth: " << firstFrame << "\n"
            << "Last frame with a defined depth: " << lastFrame << "\n"
            << "Number of frames with undefined depth: " << undefined << "\n";
  if (minVal == maxVal) {
    std::cout << "*** Warning This OFS File has a fixed depth of " << minVal
              << "! ***\n";
  }
}

// Returns m_dropframe
bool OFSFile::GetDropFrame() { return this->m_dropframe; }

// Returns a double representing the OFS's FPS value.
float OFSFile::GetOFSFps() { return this->m_fps; }

// Returns 0 if no error otherwise return.
int OFSFile::GetError() { return this->m_error; }

// Returns the filename of the OFS.
const char *OFSFile::GetFileName() { return this->m_fileName.c_str(); }

// Returns m_depths vector.
std::vector<unsigned char> OFSFile::GetDepths() { return this->m_depths; }

float OFSFpsValue2Fps(unsigned char fpsValue) {
  float fps;

  switch (fpsValue) {
    case 16:
      fps = 23.976;
      break;
    case 32:
      fps = 24;
      break;
    case 48:
      fps = 25;
      break;
    case 64:
      fps = 29.97;
      break;
    case 65:
      fps = 29.97;  // Constructor will set m_dropframe.
      break;
    case 96:
      fps = 50;
      break;
    case 112:
      fps = 59.94;
      break;
    default:
      fps = 23.976;
      std::cout << "Warning: Fps couldn't be found in OFS! ";
      std::cout << "Defaulting to 23.976.\n";
  }

  return fps;
}
