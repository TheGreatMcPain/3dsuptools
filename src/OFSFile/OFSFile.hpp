#pragma once

#include <string>
#include <vector>

class OFSFile {
 public:
  std::vector<unsigned char> GetDepths();
  int GetError();
  const char *GetFileName();
  void printOFSInfo();
  float GetOFSFps();  // Returns the FPS from the OFS File.
  bool GetDropFrame();
  OFSFile(const char *fileName);

 private:
  std::string m_fileName;
  int m_error;
  float m_fps;
  bool m_dropframe;
  std::vector<unsigned char> m_depths;
};
