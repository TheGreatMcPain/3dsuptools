#pragma once

#include <sys/stat.h>

#if defined(_WIN32)
#include <direct.h>  // _mkdir
#endif

#include <string>
#include <vector>

// Turns a string into a vector of strings. "like str.split() in Python"
std::vector<std::string> splitString(const char *string, const char splitBy);

// Joins directory with filename.
std::string joinPathFile(const char *Dir, const char *File);

// Basically basename.
std::string getBaseName(const char *Path);

// Reverse of getBaseName.
std::string getDirName(const char *Path);

// Cast float to string without trailing zeros.
std::string noZeroFloat2Str(float number);

// Handle folder creation. Returns 3 if error.
// Returns -1 if user doesn't want to overwrite existing folder.
int createFolder(const char *folderName);

// Checks if file exists then asks if user wants to overwrite it.
int fileExists(const char *fileName);
