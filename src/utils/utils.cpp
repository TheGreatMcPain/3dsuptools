#include <errno.h>

#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <utils.hpp>

// Turns a string into a vector of strings. "like str.split() in Python"
std::vector<std::string> splitString(const char *string, const char splitBy) {
  std::stringstream strStream(string);
  std::string segment;
  std::vector<std::string> seglist;

  while (std::getline(strStream, segment, splitBy)) {
    seglist.push_back(segment);
  }

  return seglist;
}

// Join file path.
std::string joinPathFile(const char *Dir, const char *File) {
  std::stringstream PathStream;
  PathStream << Dir << "/" << File;
  return PathStream.str();
}

// Basically basename.
std::string getBaseName(const char *Path) {
  std::vector<std::string> splitPath;
  splitPath = splitString(Path, '/');
  return splitPath.back();
}

std::string getDirName(const char *Path) {
  std::vector<std::string> splitPath;
  splitPath = splitString(Path, '/');
  std::stringstream outputPath;
  for (int x = 0; x < ((int)splitPath.size() - 1); x++) {
    outputPath << splitPath[x] << "/";
  }
  return outputPath.str();
}

std::string noZeroFloat2Str(float number) {
  std::ostringstream formatNumber;
  formatNumber << std::setprecision(8) << std::noshowpoint << number;
  return formatNumber.str();
}

// Checks if folder exists before creating a folder.
int createFolder(const char *folderName) {
  struct stat info;
  std::string overwrite;
  bool exists;
  int ferror;

  if (stat(folderName, &info) != 0) {
    exists = false;
  } else if (info.st_mode & S_IFDIR) {
    exists = true;
  } else {
    exists = false;
  }

  if (exists) {
    std::cout << "'" << folderName << "' Already exists! Overwrite? (y or n): ";
    std::cin >> overwrite;
    while ((overwrite.compare("y") != 0) && (overwrite.compare("n") != 0)) {
      std::cout << "Invalid input (y or n): ";
      std::cin >> overwrite;
    }

    if (overwrite.compare("n") == 0) {
      std::cout << "--- Won't overwrite: Stopping. ---\n\n";
      return -1;
    }
  }

  // Create directories.
#if !defined(_WIN32)
  ferror = mkdir(folderName, 0777);
#else
  ferror = _mkdir(folderName);
#endif

  if (errno != EEXIST) {
    if (ferror == -1) {
      std::cout << "Failed to create " << folderName << "\n";
      std::cerr << "mkdir(): " << strerror(errno) << "\n";
      return -1;
    }
  }

  return 0;
}

// Same as createFolder, but only checks files.
int fileExists(const char *fileName) {
  struct stat info;
  bool exists;
  std::string overwrite;

  stat(fileName, &info);

  if (stat(fileName, &info) != 0) {
    exists = false;
  } else if (info.st_mode & S_IFREG) {
    exists = true;
  } else {
    exists = false;
  }

  if (exists) {
    std::cout << "'" << fileName << "' Already exists! Overwrite? (y or n): ";
    std::cin >> overwrite;
    while ((overwrite.compare("y") != 0) && (overwrite.compare("n") != 0)) {
      std::cout << "Invalid input (y or n): ";
      std::cin >> overwrite;
    }

    if (overwrite.compare("n") == 0) {
      std::cout << "--- Won't overwrite: Stopping. ---\n\n";
      return -1;
    }
  }
  return 0;
}
