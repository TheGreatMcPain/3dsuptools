#pragma once

#include <tinyxml2.h>

#include <vector>

// Will Create 3D BDXML Subtitles from depth values.
class Xml3DSubtitles {
 public:
  int GetError();
  void Full3DBDXml(const char *outLeftXml, const char *outRightXml);
  void SBS3DBDXml(const char *outDir, const char *outXml, bool leftFirst);
  Xml3DSubtitles(std::vector<unsigned char> depthValues, float OFSfps,
                 const char *inDir, const char *xmlFile, const char *filter);

 private:
  int m_error;
  std::vector<unsigned char> m_depths;
  float m_OFSfps;
  tinyxml2::XMLDocument m_bdXml;
  const char *m_inDir;
  const char *m_filter;
};
