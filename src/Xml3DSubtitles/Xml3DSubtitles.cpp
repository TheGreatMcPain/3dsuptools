#include <Magick++.h>

#include <OFSFile.hpp>
#include <Xml3DSubtitles.hpp>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <utils.hpp>

#define MARGINFOR3DSUBS 5

/*
 * Function Protos
 */
int tc2Frame(const char *timecode, int framerate);
std::vector<tinyxml2::XMLElement *> graphicAttrVector(
    tinyxml2::XMLDocument *bdxml);
void replaceDepthAttrib(tinyxml2::XMLElement *graphic, int x, int y,
                        int originalDepth);
void addDepths2Xml(tinyxml2::XMLDocument *bdXml,
                   std::vector<unsigned char> depths, double framerate);

/*
 * Class functions
 */

Xml3DSubtitles::Xml3DSubtitles(std::vector<unsigned char> depthValues,
                               float OFSfps, const char *inDir,
                               const char *xmlFile, const char *filter) {
  // Initialize Class variables.
  this->m_error = 0;
  this->m_depths = depthValues;
  this->m_OFSfps = OFSfps;
  this->m_inDir = inDir;
  this->m_filter = filter;

  // Construct path to xmlFile.
  std::string xmlPath = joinPathFile(inDir, xmlFile);
  this->m_bdXml.LoadFile(xmlPath.c_str());
  if (m_bdXml.Error()) {
    // Print tinyxml2's error.
    std::cout << "From tinyxml2: \"" << m_bdXml.ErrorStr() << "\"\n";
    this->m_error = m_bdXml.ErrorID();  // Use tinyxml2 to provide error code.
    return;
  }

  std::cout << "Adding Depth values to '" << xmlPath << "'\n";
  addDepths2Xml(&this->m_bdXml, this->m_depths, this->m_OFSfps);
}

void Xml3DSubtitles::Full3DBDXml(const char *outLeftXml,
                                 const char *outRightXml) {
  tinyxml2::XMLDocument leftXml;
  tinyxml2::XMLDocument rightXml;

  // Copy to output xml objects.
  this->m_bdXml.DeepCopy(&leftXml);
  this->m_bdXml.DeepCopy(&rightXml);

  std::vector<tinyxml2::XMLElement *> inVector;
  std::vector<tinyxml2::XMLElement *> leftVector;
  std::vector<tinyxml2::XMLElement *> rightVector;

  // Create vectors for all graphic XmlElements.
  inVector = graphicAttrVector(&this->m_bdXml);
  leftVector = graphicAttrVector(&leftXml);
  rightVector = graphicAttrVector(&rightXml);

  int framewidth = 1920;
  int frameheight = 1080;

  int numSubs = 0;
  int numWarn = 0;
  int originalDepth = 0;

  std::cout << "Creating '" << outLeftXml << "' and '" << outRightXml
            << "' BDXML.\n\n";

  for (int index = 0; index < (int)inVector.size(); index++) {
    numSubs++;
    tinyxml2::XMLElement *graphic = inVector[index];

    int width = std::stoi(graphic->Attribute("Width"));
    int height = std::stoi(graphic->Attribute("Height"));
    int x = std::stoi(graphic->Attribute("X"));
    int y = std::stoi(graphic->Attribute("Y"));
    if (graphic->Attribute("Depth") != NULL) {
      originalDepth = std::stoi(graphic->Attribute("Depth"));
    } else {
      originalDepth = 0;
    }

    // Workaround for malformed subtitles as found in Amphibious 3D
    if (width == framewidth && x < 0) {
      x = 0;
    }
    if (height == frameheight && y < 0) {
      y = 0;
    }

    int depth = originalDepth;

    if (width < framewidth && width + (std::abs(depth / 2)) > framewidth) {
      int newdepth = (framewidth - width) * 2;
      if (depth < 0) {
        newdepth = newdepth * -1;
      }
      std::cout << "Warning: Subtitle " << numSubs << ": the depth (" << depth;
      std::cout << ") is too great for the width of the subtitle (" << width
                << "). ";
      std::cout << "Depth reduced to " << newdepth << "\n";
      depth = newdepth;
      numWarn++;
    }

    int leftX = x + (std::abs(depth + 1) / 2);
    int rightX = x - (std::abs(depth) / 2);

    if (rightX < 0) {
      leftX += rightX + -1;
      rightX = 0;
    }

    replaceDepthAttrib(leftVector[index], leftX, y, originalDepth);
    replaceDepthAttrib(rightVector[index], rightX, y, originalDepth);
  }

  leftXml.SaveFile(outLeftXml);
  rightXml.SaveFile(outRightXml);

  // Error Checking.
  if (leftXml.Error()) {
    // Print tinyxml2's error.
    std::cout << "From tinyxml2: \"" << m_bdXml.ErrorStr() << "\"\n";
    this->m_error = m_bdXml.ErrorID();  // Use tinyxml2 to provide error code.
  }
  if (rightXml.Error()) {
    // Print tinyxml2's error.
    std::cout << "From tinyxml2: \"" << m_bdXml.ErrorStr() << "\"\n";
    this->m_error = m_bdXml.ErrorID();  // Use tinyxml2 to provide error code.
  }
}

void Xml3DSubtitles::SBS3DBDXml(const char *outDir, const char *outXml,
                                bool leftFirst) {
  std::string xmlPath = joinPathFile(outDir, outXml);
  std::cout << "Creating Half Side-by-Side BDXML '" << xmlPath << "'\n";

  int lFirst = 1;
  if (!leftFirst) {
    lFirst = -1;
  }

  this->m_error = createFolder(outDir);
  if (this->m_error != 0) {
    return;
  }

  // Copy this->m_bdXml.
  tinyxml2::XMLDocument bdXmlCopy;
  this->m_bdXml.DeepCopy(&bdXmlCopy);
  // Create 'graphic' vector from depth bdxml.
  std::vector<tinyxml2::XMLElement *> inGraphicVector =
      graphicAttrVector(&bdXmlCopy);
  int totalSubs = inGraphicVector.size();
  int numSubs = 0;
  // Assume 1080p, because 3D BluRays don't use other resolutions.
  int frameWidth = 1920;
  int frameHeight = 1080;

  int numWarnings = 0;

  for (int index = 0; index < totalSubs; index++) {
    tinyxml2::XMLElement *graphic = inGraphicVector[index];
    numSubs++;

    // Initialize variables from xml attribs.
    int width = std::stoi(graphic->Attribute("Width"));
    int height = std::stoi(graphic->Attribute("Height"));
    int x = std::stoi(graphic->Attribute("X"));
    int y = std::stoi(graphic->Attribute("Y"));

    int origDepth;
    if (graphic->Attribute("Depth") != NULL) {
      origDepth = std::stoi(graphic->Attribute("Depth"));
    } else {
      origDepth = 0;
    }

    // Fix for malformed 3D subtitles.
    if (width == frameWidth && x < 0) {
      x = 0;
    }
    if (height == frameHeight && y < 0) {
      y = 0;
    }

    const char *pngFileIn = graphic->GetText();
    int depth = origDepth * lFirst;

    if (width < frameWidth && width + (std::abs(depth / 2)) > frameWidth) {
      int newdepth = (frameWidth - width) * 2;
      if (depth < 0) {
        newdepth = newdepth * -1;
      }
      std::cout << "Warning: Subtitle " << numSubs << ": the depth (" << depth;
      std::cout << ") is too great for the width of the subtitle (" << width
                << "). ";
      std::cout << "Depth reduced to " << newdepth << "\n";
      depth = newdepth;
      numWarnings++;
    }

    int newWidth = width + frameWidth - depth;
    int newHeight = height;
    // gravity1 = left side, gravity2 = right side.
    Magick::GravityType gravity1 = Magick::GravityType::WestGravity;
    Magick::GravityType gravity2 = Magick::GravityType::EastGravity;
    // Resize image to half it's width.
    Magick::Geometry resize("50%x100%!");
    int finalWidth = (newWidth + 1) / 2;
    int finalHeight = height;
    int newx = std::ceil(((float)x + ((float)depth / 2)) / 2);
    int newy = y;

    std::ostringstream fmtNumSubs;
    std::stringstream outPngStream;
    fmtNumSubs << std::setfill('0') << std::setw(4) << numSubs;
    outPngStream << "sub_" << fmtNumSubs.str() << ".png";
    std::string outPng = outPngStream.str();

    std::string inPath = joinPathFile(this->m_inDir, pngFileIn);
    std::string outPath = joinPathFile(outDir, outPng.c_str());

    // Create input image, and output (background) image.
    Magick::Image inPng(inPath);
    Magick::Image background;
    // Set background format to Canvas.
    background.magick("XC");
    background.size(Magick::Geometry(newWidth, newHeight));
    // Make bdsup2sub++ happy.
    background.depth(8);
    background.alpha(true);
    background.backgroundColor("gray50");
    background.erase();
    background.composite(inPng, gravity1);
    background.composite(inPng, gravity2);
    background.transparent("gray50");
    if (std::strncmp(this->m_filter, "mitchell", 8) == 0) {
      background.filterType(Magick::FilterType::MitchellFilter);
    } else if (std::strncmp(this->m_filter, "lanczos3", 8) == 0) {
      background.filterType(Magick::FilterType::LanczosFilter);
    }
    background.resize(resize);

    // Handle 'right first'?
    if (newx < 0) {
      int cropx = newx * -1;
      finalWidth += newx;
      newx = 0;
      if (finalWidth > frameWidth) {
        finalWidth = frameWidth;
      }
      std::stringstream cropstream;
      cropstream << finalWidth << "x" << finalHeight << "+";
      cropstream << cropx << "+0";
      background.crop(Magick::Geometry(cropstream.str()));
    }

    // Write new subtitle image to disk.
    background.magick("PNG");
    background.write(outPath);

    // Set new XML Attributes
    graphic->SetAttribute("Width", std::to_string(finalWidth).c_str());
    graphic->SetAttribute("Height", std::to_string(finalHeight).c_str());
    graphic->SetAttribute("X", std::to_string(newx).c_str());
    graphic->SetAttribute("Y", std::to_string(newy).c_str());
    graphic->DeleteAttribute("Depth");
    graphic->SetAttribute("OrigDepth", std::to_string(origDepth).c_str());

    // Set new png file.
    graphic->SetText(outPng.c_str());

    // Print status.
    std::stringstream status;
    status << numSubs << " Subs out of " << totalSubs << " Subs";
    std::cout << status.str() << "\r" << std::flush;
  }
  std::cout << "\n\n";

  bdXmlCopy.SaveFile(xmlPath.c_str());

  if (bdXmlCopy.Error()) {
    // Print tinyxml2's error.
    std::cout << "From tinyxml2: \"" << bdXmlCopy.ErrorStr() << "\"\n";
    this->m_error = m_bdXml.ErrorID();  // Use tinyxml2 to provide error code.
    return;
  }
}

int Xml3DSubtitles::GetError() { return this->m_error; }

/*
 * Utility Functions.
 */

// Replaces 'Depth' attribute with 'OriginalDepth'.
void replaceDepthAttrib(tinyxml2::XMLElement *graphic, int x, int y,
                        int originalDepth) {
  std::string strX = std::to_string(x);
  std::string strY = std::to_string(y);
  std::string strOrigDepth = std::to_string(originalDepth);

  graphic->SetAttribute("X", strX.c_str());
  graphic->SetAttribute("Y", strY.c_str());

  if (graphic->Attribute("Depth") != NULL) {
    graphic->DeleteAttribute("Depth");
  }
  graphic->SetAttribute("OriginalDepth", strOrigDepth.c_str());
}

// Puts Graphic XMLElements into a vector.
std::vector<tinyxml2::XMLElement *> graphicAttrVector(
    tinyxml2::XMLDocument *bdxml) {
  std::vector<tinyxml2::XMLElement *> outVector;

  tinyxml2::XMLElement *events =
      bdxml->FirstChildElement("BDN")->FirstChildElement("Events");
  for (tinyxml2::XMLElement *child = events->FirstChildElement(); child != NULL;
       child = child->NextSiblingElement()) {
    tinyxml2::XMLElement *graphic = child->FirstChildElement("Graphic");
    outVector.push_back(graphic);
  }

  return outVector;
}

// Adds 'Depth' attributes to BDXML.
void addDepths2Xml(tinyxml2::XMLDocument *bdXml,
                   std::vector<unsigned char> depths, double framerate) {
  int fps;
  double roundfps = round(framerate * 1000) / 1000;
  if (roundfps == 23.976) {
    fps = 24;
  } else if (roundfps == 24) {
    fps = 24;
  } else if (roundfps == 25) {
    fps = 25;
  } else if (roundfps == 29.97) {
    fps = 30;
  } else if (roundfps == 50) {
    fps = 50;
  } else if (roundfps == 59.94) {
    fps = 60;
  }

  int numOfFrames = depths.size();

  int numsubs = 0;
  int numwarnings = 0;
  int mindepth = 300;
  int maxdepth = -300;
  int alldepths = 0;
  int inframe;
  int outframe;

  tinyxml2::XMLElement *events =
      bdXml->FirstChildElement("BDN")->FirstChildElement("Events");

  for (tinyxml2::XMLElement *child = events->FirstChildElement(); child != NULL;
       child = child->NextSiblingElement()) {
    numsubs++;
    inframe = tc2Frame(child->Attribute("InTC"), fps);
    outframe = tc2Frame(child->Attribute("OutTC"), fps);
    tinyxml2::XMLElement *graphic = child->FirstChildElement("Graphic");

    if (inframe >= numOfFrames) {
      graphic->SetAttribute("Depth", "0");
      std::string s = std::to_string(outframe - inframe);
      graphic->SetAttribute("DepthWarning", s.c_str());
      numwarnings++;
    } else {
      int margin = outframe - inframe / 6;
      if (margin > MARGINFOR3DSUBS) {
        margin = MARGINFOR3DSUBS;
      }

      std::vector<unsigned char> p(depths.begin() + (inframe + margin),
                                   depths.begin() + (outframe - margin + 1));
      int depth = -300;
      int n = p.size();

      int warn = 0;
      int cuts = 0;

      unsigned char val = p[0];

      for (int i = 0; i < n; i++) {
        unsigned char h = p[i];
        if (h != val) {
          cuts++;
          val = h;
        }
        if (h == 128) {
          warn++;
        } else {
          int v = (h & 127);
          if ((h & 128) / 128) {
            v = -v;
          }
          v = v * 2;
          if (v > depth) {
            depth = v;
          }
        }
      }

      if (depth == -300) {
        depth = 0;
      } else {
        alldepths += depth;
        if (depth < mindepth) {
          mindepth = depth;
        }
        if (depth > maxdepth) {
          maxdepth = depth;
        }
      }

      std::string s = std::to_string(depth);
      graphic->SetAttribute("Depth", s.c_str());
      if (warn > 0) {
        std::string s = std::to_string(warn);
        graphic->SetAttribute("UndefinedFrameDepth", s.c_str());
        numwarnings++;
      }
    }
  }

  double avgdepth = round((double)alldepths / (double)numsubs * 100) / 100;
  std::cout << numsubs << " Subtitles, " << numwarnings << " subtitles with ";
  std::cout << "undefined depth values, average depth: " << avgdepth;
  std::cout << ", minimum depth: " << mindepth;
  std::cout << ", maximum depth: " << maxdepth << "\n\n";
}

// Converts the timecodes found on BDXML's to a frame number.
// Timecode layout: hr:min:sec:frame
int tc2Frame(const char *timecode, int framerate) {
  std::vector<std::string> tc = splitString(timecode, ':');
  std::string ff = tc.back();

  int hours = std::stoi(tc[0]) * 60 * 60;
  int minutes = std::stoi(tc[1]) * 60;
  int secs = hours + minutes + std::stoi(tc[2]);

  return floor(secs * framerate) + std::stoi(ff);
}
