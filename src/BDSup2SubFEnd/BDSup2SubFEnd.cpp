#include <BDSup2SubFEnd.hpp>
#include <stdoutredirect.hpp>
#include <utils.hpp>

BDSup2SubFEnd::BDSup2SubFEnd(float fps, const char *langCode,
                             const char *filter) {
  this->m_bdsup2sub = new BDSup2Sub;
  this->m_lang = langCode;
  this->m_fps = fps;
  this->m_error = 0;
  this->m_filter = filter;
  this->m_verbose = false;
}

void BDSup2SubFEnd::convertSub(const char *output, const char *input) {
  std::string fpsString = noZeroFloat2Str(m_fps);

  std::vector<const char *> cmd;
  cmd.push_back("bdsup2sub++");
  cmd.push_back("--no-verbose");
  cmd.push_back("--fps-target");
  cmd.push_back(fpsString.c_str());
  cmd.push_back("--filter");
  cmd.push_back(m_filter);
  cmd.push_back("--language");
  cmd.push_back(m_lang);
  cmd.push_back("--output");
  cmd.push_back(output);
  cmd.push_back(input);

  std::cout << "Equivalent command: ";

  for (int x = 0; x < (int)cmd.size(); x++) {
    std::cout << cmd[x] << " ";
  }
  std::cout << "\n\n";

  int cmdc = cmd.size();

  // Execute bdsup2sub++ (Redirect stdout)
  stdoutRedirect stdoutRdct;
  if (!this->m_verbose) stdoutRdct.StartRedirect();
  bool bdsup2sub_error = this->m_bdsup2sub->execCLI(cmdc, (char **)cmd.data());
  if (!this->m_verbose) stdoutRdct.StopRedirect();
  if (bdsup2sub_error) {
    this->m_error = 3;
  }
}

void BDSup2SubFEnd::SetVerbose(bool verbose) { this->m_verbose = verbose; }

int BDSup2SubFEnd::GetError() { return this->m_error; }
