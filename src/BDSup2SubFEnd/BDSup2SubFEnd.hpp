#pragma once

#include <OFSFile.hpp>
#include <QtGlobal>
#include <iostream>

#if QT_VERSION >= 0x050000
#include <QtWidgets/QApplication>
#else
#include <QtGui/QApplication>
#endif

#include <bdsup2sub.h>
#ifdef Q_OS_WIN
#include <windows.h>

#include <QtPlugin>
Q_IMPORT_PLUGIN(QWindowsIntegrationPlugin)
#endif

class BDSup2SubFEnd {
 public:
  int GetError();
  void convertSub(const char *output, const char *input);
  void SetVerbose(bool verbose);
  BDSup2SubFEnd(float fps, const char *langCode, const char *filter);

 private:
  BDSup2Sub *m_bdsup2sub;
  float m_fps;
  const char *m_lang;
  const char *m_filter;
  int m_error;
  bool m_verbose;
};
