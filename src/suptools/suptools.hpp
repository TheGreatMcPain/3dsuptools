#pragma once

#include <BDSup2SubFEnd.hpp>
#include <OFSFile.hpp>
#include <Xml3DSubtitles.hpp>
#include <string>

class suptools {
 public:
  void fullSBS(const char *inputSup, const char *outputSup);
  void halfSBS(const char *inputSup, const char *outputSup);
  void execute();
  int GetError();
  suptools(int argc, char *argv[]);
  ~suptools();

 private:
  void parseArgs(int argc, char *argv[]);
  BDSup2SubFEnd *m_bdsup2sub;
  OFSFile *m_ofsFile;
  int m_error;
  bool m_execute;
  bool m_leftFirst;
  bool m_BDverbose;
  std::string m_mode;
  std::string m_filter;
  std::string m_inputSup;
  std::string m_outputSup;
  std::string m_langCode;
};
