#include <cxxopts.hpp>
#include <suptools.hpp>
#include <utils.hpp>

#include "BDSup2SubFEnd.hpp"
#include "OFSFile.hpp"

#define VERSION "v0.1"
#define YEAR "2020"

void printIntroPart1() {
  std::cout << "3DSupTools " << VERSION << " Copyright (C) " << YEAR
            << " TheGreatMcPain (aka Sixsupersonic on doom9)\n";
  std::cout << "This binary was compiled on: " << __DATE__ << " " << __TIME__
            << "\n\n";
}

void printIntroPart2() {
  // GPL Stuff
  std::cout << "This program is licensed under GPLv3.\n";
  std::cout << "For more information; use option '--license'\n\n";
}

void printLicense() {
  std::cout
      << "3DSupTools is free software: you can redistribute it and/or\n"
      << "modify it under the terms of the GNU General Public License\n"
      << "as published by the Free Software Foundation, either\n"
      << "version 3 of the License, or (at your option) any later version.\n\n"
      << "Foobar is distributed in the hope that it will be useful,\n"
      << "but WITHOUT ANY WARRANTY; without even the implied warranty\n"
      << "of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See\n"
      << "the GNU General Public License for more details.\n\n"
      << "You should have received a copy of the GNU General Public\n"
      << "License along with 3DSupTools.\n"
      << "If not, see http://www.gnu.org/licenses/.\n\n";
}

suptools::suptools(int argc, char *argv[]) {
  printIntroPart1();
  this->m_error = 0;
  this->m_leftFirst = true;
  this->m_execute = true;

  this->parseArgs(argc, argv);

  if (!this->m_execute) {
    return;
  }

  this->m_bdsup2sub =
      new BDSup2SubFEnd(this->m_ofsFile->GetOFSFps(), this->m_langCode.c_str(),
                        this->m_filter.c_str());
}

suptools::~suptools() {
  if (this->m_execute) {
    delete (this->m_bdsup2sub);
    delete (this->m_ofsFile);
  }
}

void suptools::execute() {
  if (!this->m_execute) {
    return;
  }
  printIntroPart2();

  if (this->m_mode.compare("ofsinfo") == 0) {
    this->m_ofsFile->printOFSInfo();
  } else if (this->m_mode.compare("halfsbs") == 0) {
    this->halfSBS(this->m_inputSup.c_str(), this->m_outputSup.c_str());
  } else if (this->m_mode.compare("fullsbs") == 0) {
    this->fullSBS(this->m_inputSup.c_str(), this->m_outputSup.c_str());
  }
}

void suptools::parseArgs(int argc, char *argv[]) {
  try {
    cxxopts::Options options("3Dsuptools",
                             "BDSUP 3D Convertion tool (based on BD3D2MK3D)");

    // Clang format will fuck up the readability of this chunk.
    // clang-format off
    options.add_options()
      ("ofs", "Input 3D Plane file 'OFS'", cxxopts::value<std::string>(),
                                           "<3D-Plane.ofs>")
      ("mode", "Operation mode", cxxopts::value<std::string>(),
                                      "<ofsinfo, fullsbs, or halfsbs>")
      ("input-sup", "Input BDSUP file", cxxopts::value<std::string>(),
                                         "<input.sup>")
      ("output-sup", "Converted BDSUP output file",
                                      cxxopts::value<std::string>(),
                                      "<output.sup>")
      ("lang", "BDSup2Sub language code 'Default: en'",
                            cxxopts::value<std::string>()->default_value("en"),
                            "<2 character language code>")
      ("filter", "Resize filter 'Default: mitchell'",
                      cxxopts::value<std::string>()->default_value("mitchell"),
                      "<mitchell, or lanczos3>")
      ("verbose", "Print bdsup2sub++'s log.")
      ("rightfirst", "Make right-side first")
      ("help", "Print this message")
      ("license", "Print license infomation.")
      ;
    // clang-format on

    auto result = options.parse(argc, argv);
    auto arguments = result.arguments();

    // Parse/Initalize options.
    if (result.count("help") || arguments.size() == 0) {
      printIntroPart2();
      std::cout << options.help() << "\n";
      this->m_execute = false;
      return;
    }

    if (result.count("license")) {
      printLicense();
      this->m_execute = false;
      return;
    }

    if (result.count("ofs")) {
      std::string ofsFileName;
      ofsFileName = result["ofs"].as<std::string>();
      this->m_ofsFile = new OFSFile(ofsFileName.c_str());
      if (this->m_ofsFile->GetError() != 0) {
        exit(this->m_ofsFile->GetError());
      }
    } else {
      std::cout << "Argument 'ofs' is required.\n";
      exit(1);
    }

    if (result.count("mode")) {
      this->m_mode = result["mode"].as<std::string>();
      if (this->m_mode.compare("ofsinfo") == 0) {
        // ofsinfo doesn't need any extra options.
        return;
      }
      if (this->m_mode.compare("fullsbs") != 0 &&
          this->m_mode.compare("halfsbs") != 0) {
        std::cout << "Invalid mode option\n";
        exit(1);
      }
    } else {
      std::cout << "Argument 'mode' is required.\n";
      exit(1);
    }

    this->m_BDverbose = false;
    if (result.count("verbose")) {
      this->m_BDverbose = true;
    }

    this->m_langCode = result["lang"].as<std::string>();

    if (result.count("rightfirst")) {
      this->m_leftFirst = false;
    }

    this->m_filter = result["filter"].as<std::string>();
    if (result.count("filter")) {
      if (this->m_filter.compare("mitchell") != 0 &&
          this->m_filter.compare("lanczos3") != 0) {
        std::cout << "Invalid filter option\n";
        exit(1);
      }
    }

    if (result.count("input-sup")) {
      this->m_inputSup = result["input-sup"].as<std::string>();
    } else if (this->m_mode.compare("ofsinfo") != 0) {
      std::cout << "Argument 'input-sup' is required.\n";
      exit(1);
    }

    if (result.count("output-sup")) {
      this->m_outputSup = result["output-sup"].as<std::string>();
    } else if (this->m_mode.compare("ofsinfo") != 0) {
      std::cout << "Argument 'output-sup' is required.\n";
      exit(1);
    }

  } catch (const cxxopts::OptionException &e) {
    std::cout << e.what() << "\n";
    exit(1);
  }
}

void printStep(int step, const int steps) {
  std::cout << "--- Step " << step << "/" << steps << " ---\n";
}

void suptools::fullSBS(const char *inputSup, const char *outputSup) {
  int step = 0;
  const int steps = 5;

  // Get mono BDXML folder name.
  std::string inputFileName = getBaseName(inputSup);
  std::string inputBase = splitString(inputFileName.c_str(), '.')[0];
  std::string BDXmlDir = inputBase + ".2D";

  printStep(++step, steps);
  // Create directory for BDXML.
  std::cout << "Creating Initial BDXML.\n";
  if ((this->m_error = createFolder(BDXmlDir.c_str())) != 0) {
    if (this->m_error == -1) {
      this->m_error = 0;
    }
    return;
  }

  // Create BDXML
  std::string inputXml = inputBase + ".xml";
  std::string outputPath = joinPathFile(BDXmlDir.c_str(), inputXml.c_str());
  if (this->m_BDverbose) {
    this->m_bdsup2sub->SetVerbose(true);
  }

  this->m_bdsup2sub->convertSub(outputPath.c_str(), inputSup);

  // Add .left. and .right. to output filename.
  std::string outputBase = getBaseName(outputSup);
  std::vector<std::string> outputStrVec = splitString(outputBase.c_str(), '.');
  std::stringstream outputPrimaryStrm;
  std::stringstream outputSecondaryStrm;

  for (int x = 0; x < (int)(outputStrVec.size() - 1); x++) {
    outputPrimaryStrm << outputStrVec[x];
    outputSecondaryStrm << outputStrVec[x];
  }

  std::string PrimarySide;
  std::string SecondarySide;
  if (this->m_leftFirst) {
    PrimarySide = "left";
    SecondarySide = "right";
  } else {
    PrimarySide = "right";
    SecondarySide = "left";
  }

  outputPrimaryStrm << "." << PrimarySide << "." << outputStrVec.back();
  outputSecondaryStrm << "." << SecondarySide << "." << outputStrVec.back();

  std::string outputPrimarySup =
      getDirName(outputSup) + outputPrimaryStrm.str();
  std::string outputSecondarySup =
      getDirName(outputSup) + outputSecondaryStrm.str();

  // Create Primary/Secondary BDXML
  printStep(++step, steps);
  Xml3DSubtitles xml3DSubs(this->m_ofsFile->GetDepths(),
                           this->m_ofsFile->GetOFSFps(), BDXmlDir.c_str(),
                           inputXml.c_str(), this->m_filter.c_str());

  std::string primaryXml = PrimarySide + "-" + inputXml;
  std::string secondaryXml = SecondarySide + "-" + inputXml;
  std::string primaryXmlPath =
      joinPathFile(BDXmlDir.c_str(), primaryXml.c_str());
  std::string secondaryXmlPath =
      joinPathFile(BDXmlDir.c_str(), secondaryXml.c_str());

  printStep(++step, steps);
  xml3DSubs.Full3DBDXml(primaryXmlPath.c_str(), secondaryXmlPath.c_str());

  // Create new sup files.
  printStep(++step, steps);
  if ((this->m_error = fileExists(outputPrimarySup.c_str())) != 0) {
    if (this->m_error == -1) {
      this->m_error = 0;
    }
    return;
  }
  std::cout << "Creating '" << outputPrimarySup << "'\n";
  this->m_bdsup2sub->convertSub(outputPrimarySup.c_str(),
                                primaryXmlPath.c_str());
  printStep(++step, steps);
  if ((this->m_error = fileExists(outputSecondarySup.c_str())) != 0) {
    if (this->m_error == -1) {
      this->m_error = 0;
    }
    return;
  }
  std::cout << "Creating '" << outputSecondarySup << "'\n";
  this->m_bdsup2sub->convertSub(outputSecondarySup.c_str(),
                                secondaryXmlPath.c_str());
}

void suptools::halfSBS(const char *inputSup, const char *outputSup) {
  int step = 0;
  const int steps = 4;

  // Get BDXML folder names.
  std::string inputFileName = getBaseName(inputSup);
  std::string inputBase = splitString(inputFileName.c_str(), '.')[0];
  std::string BDXmlDir = inputBase + ".2D";
  std::string SBSXmlDir = inputBase + ".3D";

  printStep(++step, steps);
  // Create directory for BDXML.
  std::cout << "Creating Initial BDXML.\n";
  if ((this->m_error = createFolder(BDXmlDir.c_str())) != 0) {
    if (this->m_error == -1) {
      this->m_error = 0;
    }
    return;
  }

  // Create BDXML
  std::string inputXml = inputBase + ".xml";
  std::string outputPath = joinPathFile(BDXmlDir.c_str(), inputXml.c_str());
  if (this->m_BDverbose) {
    this->m_bdsup2sub->SetVerbose(true);
  }

  this->m_bdsup2sub->convertSub(outputPath.c_str(), inputSup);

  // Generate SBS BDXML
  std::string SBSXml = inputBase + ".3D" + ".xml";

  printStep(++step, steps);
  Xml3DSubtitles xml3DSubs(this->m_ofsFile->GetDepths(),
                           this->m_ofsFile->GetOFSFps(), BDXmlDir.c_str(),
                           inputXml.c_str(), this->m_filter.c_str());

  printStep(++step, steps);
  xml3DSubs.SBS3DBDXml(SBSXmlDir.c_str(), SBSXml.c_str(), this->m_leftFirst);
  if ((this->m_error = xml3DSubs.GetError()) != 0) {
    if (this->m_error == -1) {
      this->m_error = 0;
    }
    return;
  }

  // Create New BDSUP
  std::string SBSXmlPath;
  SBSXmlPath = joinPathFile(SBSXmlDir.c_str(), SBSXml.c_str());
  printStep(++step, steps);
  if ((this->m_error = fileExists(outputSup)) != 0) {
    if (this->m_error == -1) {
      this->m_error = 0;
    }
    return;
  }
  std::cout << "Creating '" << outputSup << "'\n";
  this->m_bdsup2sub->convertSub(outputSup, SBSXmlPath.c_str());
}

int suptools::GetError() { return this->m_error; }
