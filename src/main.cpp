#include <iostream>
#include <suptools.hpp>

int main(int argc, char *argv[]) {
  // Make QT Happy. (BDSup2Sup++ needs QT)
  QApplication a(argc, (char **)argv);

  suptools suptools(argc, argv);
  suptools.execute();
}
